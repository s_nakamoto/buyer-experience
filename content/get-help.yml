---
  title: Get help with GitLab
  description: Get info on technical support, updating your GitLab instance, feature proposals and bug tracking.
  side_menu:
    links:
      - text: Account Support
        href: "#account-support"
        data_ga_name: account support
        data_ga_location: header
      - text: Contributing
        href: "#contributing"
        data_ga_name: contributing
        data_ga_location: header
      - text: Feature Proposals
        href: "#feature-proposals"
        data_ga_name: feature proposals
        data_ga_location: header
      - text: References
        href: "#references"
        data_ga_name: references
        data_ga_location: header
      - text: Licensing and Subscriptions
        href: "#licensing-and-subscriptions"
        data_ga_name: licensing and subscriptions
        data_ga_location: header
      - text: Reproducible Bugs
        href: "#reproducible-bugs"
        data_ga_name: reproducible bugs
        data_ga_location: header
      - text: Other resources for discussion
        href: "#other-resources-for-discussion"
        data_ga_name: other resources for discussion
        data_ga_location: header
      - text: Security
        href: "#security"
        data_ga_name: security
        data_ga_location: header
      - text: Technical Support
        href: "#technical-support"
        data_ga_name: technical support
        data_ga_location: header
      - text: Updating
        href: "#updating"
        data_ga_name: updating
        data_ga_location: header
  components:
    - name: call-to-action
      data:
        title: Help Topics
        horizontal_rule: true
        centered_by_default: true
    - name: copy-media
      data:
        block:
          - header: Account Support
            miscellaneous: |
              If you haven't received your confirmation email, you can request to resend your confirmation instructions via our [confirmation page](https://gitlab.com/users/confirmation/new){data-ga-name="confirmation page" data-ga-location="body"}.
            metadata:
              id_tag: account-support
    - name: copy-media
      data:
        block:
          - header: Contributing
            miscellaneous: |
              Our development documentation describes how to submit [merge requests](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/contributing/merge_request_workflow.md){data-ga-name="merge request" data-ga-location="body"} and [issues](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/contributing/issue_workflow.md){data-ga-name="issues" data-ga-location="contributing"}. Merge requests and issues not in line with the guidelines in these documents will be closed.
            metadata:
              id_tag: contributing
    - name: copy-media
      data:
        block:
          - header: Feature Proposals
            miscellaneous: |
              Feature proposals should be submitted to the [issue tracker](https://gitlab.com/gitlab-org/gitlab/issues){data-ga-name="issue tracker" data-ga-location="feature proposals"}.

              Please read the [contributing guidelines for feature proposals](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/contributing/issue_workflow.md#feature-proposals){data-ga-name="feature proposals" data-ga-location="body"} before posting on the Issue tracker and make use of the "Feature Proposal" issue template.

            metadata:
              id_tag: feature-proposals
    - name: copy-media
      data:
        block:
          - header: References
            miscellaneous: |
              *  [GitLab Documentation](https://docs.gitlab.com/){data-ga-name="documentation" data-ga-location="body"}: documents all GitLab applications.
              *  [GitLab Forum](https://forum.gitlab.com/){data-ga-name="forum" data-ga-location="references"}: get help directly from the community.
              *  [GitLab University](https://docs.gitlab.com/ee/university/){data-ga-name="university" data-ga-location="body"}: contains a variety of resources for learning Git and GitLab.
              *  [The GitLab Cookbook](https://www.packtpub.com/application-development/gitlab-cookbook): written by core team member Jeroen van Baarsen, it is the most comprehensive book about GitLab.
              *  [GitLab Recipes](https://gitlab.com/gitlab-org/gitlab-recipes){data-ga-name="recipes" data-ga-location="body"}: A range of useful unofficial guides to using non-packaged software in conjunction with GitLab.
              *  [Learn Enough Git to Be Dangerous by Michael Hartl](http://www.learnenough.com/git-tutorial): is a great introduction to version control and git.
              *  Version two of the [Pro Git book](http://git-scm.com/book/en/v2) has a [section about GitLab](http://git-scm.com/book/en/v2/Git-on-the-Server-GitLab).
              *  O'Reilly Media 'Git for teams' [book](http://shop.oreilly.com/product/0636920034520.do) has a chapter on GitLab. There are also [videos](http://shop.oreilly.com/product/0636920034872.do?code=WKGTVD) about Git and GitLab. They also provide a [free video about creating a GitLab account](http://player.oreilly.com/videos/9781491912003?toc_id=194077).
              *  [GitLab YouTube Channel](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg): the place where you can see videos of features and installation options.
            metadata:
              id_tag: references
    - name: copy-media
      data:
        block:
          - header: Licensing and Subscriptions
            miscellaneous: |
              [FAQ about purchasing, renewals, payment and billable users](/pricing/licensing-faq/){data-ga-name="FAQ about purchasing" data-ga-location="body"}
            metadata:
              id_tag: licensing-and-subscriptions
    - name: copy-media
      data:
        block:
          - header: Reproducible Bugs
            miscellaneous: |
              Bug reports should be submitted to the [issue tracker](https://gitlab.com/gitlab-org/gitlab/issues){data-ga-name="issue tracker" data-ga-location="reproducible bugs"}.

              Please read the [contributing guidelines for reporting bugs](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/contributing/issue_workflow.md){data-ga-name="issues" data-ga-location="reproducible bugs"} before posting on the Issue tracker and make use of the "Bug" issue template.

            metadata:
              id_tag: reproducible-bugs
    - name: copy-media
      data:
        block:
          - header: Other community run resources for discussion
            miscellaneous: |
              * [GitLab Community Forum](https://forum.gitlab.com/){data-ga-name="forum" data-ga-location="references"}: this is the best place to have a discussion.
              * [Gitter chat room](https://gitter.im/gitlab/home): here you can ask questions when you need help.
              * [Discord chat room](https://discord.gg/uCKTg3fTQ6): here you can ask questions when you need help.
              * [Reddit](https://www.reddit.com/r/gitlab/)
              * [Stack Overflow](http://stackoverflow.com/questions/tagged/gitlab): Please search for similar issues before posting your own, as there's a good chance somebody else had the same issue as you and has already found a solution.
              * [#gitlab Libera IRC channel](https://web.libera.chat/#gitlab): a Libera channel to get in touch with other GitLab users and get help.

            metadata:
              id_tag: other-resources-for-discussion
    - name: copy-media
      data:
        block:
          - header: Security
            miscellaneous: |
              * [The responsible disclosure page describes](/security/disclosure/){data-ga-name="security disclosure" data-ga-location="body"} how to contact GitLab to report security vulnerabilities and other security information.
              * [The security section in the documentation](http://doc.gitlab.com/ce/security/README.html){data-ga-name="security documentation" data-ga-location="body"} lists what you can do to further secure your GitLab instance.
              * [The Trust & Safety page](/handbook/engineering/security/security-operations/trustandsafety/#how-to-report-abuse){data-ga-name="trust and safety" data-ga-location="body"} describes how to contact GitLab to report abuse on the platform, including phishing pages, malware, and DMCA requests.
              * [The GitLab Trust Center](/security/){data-ga-location="trust center" data-ga-location="body"} provides our security and privacy FAQs, as well as how to contact our security team.

            metadata:
              id_tag: security
    - name: copy-media
      data:
        block:
          - header: Technical Support
            miscellaneous: |
              For details on where to get Technical Support, and with what level of service, please see the [Support page](/support/){data-ga-name="support" data-ga-location="body"}.

            metadata:
              id_tag: technical-support
    - name: copy-media
      data:
        block:
          - header: Updating
            miscellaneous: |
              * [GitLab update page](/update/){data-ga-name="update" data-ga-location="body"}: resources and information to help you update your GitLab instance.
              * [Maintenance policy](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/MAINTENANCE.md){data-ga-name="maintanance policy" data-ga-location="body"}: specifies what versions are supported.

            metadata:
              id_tag: updating
