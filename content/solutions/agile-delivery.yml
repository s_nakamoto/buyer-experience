---
  title: GitLab Agile Planning
  description: How to use GitLab as an agile project management tool for agile processes such as Scrum, Kanban, and Scaled Agile Framework (SAFe).
  components:
    - name: 'solutions-hero'
      data:
        title: Agile Planning
        subtitle: Plan and manage your projects, programs, and products with integrated Agile support
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          text: Start your free trial
          url: /free-trial/
        secondary_btn:
          text: Learn about pricing
          url: /pricing/
          data_ga_name: Learn about pricing
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          image_url_mobile: /nuxt-images/solutions/no-image-mobile.svg
          alt: "Image: gitlab for public sector"
    - name: 'copy-media'
      data:
        block:
          - header: Agile planning with Gitlab
            text: |
              Development teams continue to accelerate value delivery with iterative, incremental, and lean project methodologies, such as Scrum, Kanban, and Extreme Programming (XP). Large enterprises have adopted Agile at enterprise scale through a variety of frameworks, including Scaled Agile Framework (SAFe), Spotify, and Large Scale Scrum (LeSS). GitLab enables teams to apply Agile practices and principles to organize and manage their work, whatever their chosen methodology.
            link_href: /demo/
            link_text: Learn more
            video:
              video_url: https://www.youtube.com/embed/wmtZKC8m2ew?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - header: GitLab benefits
            text: |
              As a single application for the complete DevOps lifecycle, GitLab is:

              *   **Seamless:** GitLab supports collaboration and visibility for Agile teams — from planning to deployment and beyond — with a single user experience and a common set of tools
              *   **Integrated:** Manage projects in the same system where you perform your work
              *   **Scalable:** Organize multiple Agile teams to achieve enterprise Agile scalability
              *   **Flexible:** Customize out-of-the-box functionality to the needs of your methodology, whether you're rolling your own flavor of Agile or adopting a formal framework
              *   **Easy to learn:** See our [Quick Start](https://www.youtube.com/watch?v=VR2r1TJCDew){data-ga-name="quick start" data-ga-location="body"} guide on setting up Agile teams
            image:
              image_url: /nuxt-images/blogimages/cicd_pipeline_infograph.png
              alt: ""
          - header: Manage Agile projects
            inverted: true
            text: |
              GitLab enables lean and Agile project management from basic issue tracking to Scrum and Kanban-style project management. Whether you’re simply tracking a few issues or managing the complete DevOps lifecycle across a team of developers, GitLab has your team covered.

              *   **Plan, assign, and track** with [issues](https://docs.gitlab.com/ee/user/project/issues/index.html){data-ga-name="issues" data-ga-location="body"}
              *   **Organize work** with [labels](https://docs.gitlab.com/ee/user/project/labels.html){data-ga-name="labels" data-ga-location="body"}, [iterations](https://docs.gitlab.com/ee/user/group/iterations/){data-ga-name="iterations" data-ga-location="body"} and [milestones](https://docs.gitlab.com/ee/user/project/milestones/index.html#overview){data-ga-name="milestones" data-ga-location="body"}
              *   **Visualize work** with [boards](https://docs.gitlab.com/ee/user/project/issue_board.html#issue-boards){data-ga-name="boards" data-ga-location="body"}
              *   **Correlate work with output** using [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/index.html){data-ga-name="merge requests" data-ga-location="body"}
            video:
              video_url: https://www.youtube.com/embed/wmtZKC8m2ew?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - header: Manage programs and portfolios
            text: |
              Maintain visibility and control the people and projects aligned with business initiatives. Define and enforce policy and permissions, track progress and velocity across multiple projects and groups, and prioritize initiatives to deliver the greatest amount of value.

              *   **Organize** new business initiatives and efforts into [Epics](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="epics" data-ga-location="body"}
              *   **Align teams** and projects into programs—without sacrificing security and visibility—using Subgroups
              *   **Plan** [Sub-Epics](https://docs.gitlab.com/ee/user/group/epics/manage_epics.html#multi-level-child-epics){data-ga-name="sub-epics" data-ga-location="body"} and issues into Iterations and Milestones
              *   **Visualize** value delivery using Roadmaps, [Insights](https://docs.gitlab.com/ee/user/project/insights/){data-ga-name="insights" data-ga-location="body"}, and [Value Stream Analytics](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html){data-ga-name="value stream analytics" data-ga-location="body"}
            video:
              video_url: https://www.youtube.com/embed/VR2r1TJCDew?enablejsapi=1&amp;origin=https%3A%2F%2Fabout.gitlab.com
          - header: Scaled Agile Framework (SAFe) with GitLab
            inverted: true
            text: |
              See how your organization can use GitLab to build a framework using the Scaled Agile Framework (SAFe). Dive into the details around building out your Agile framework for development teams built on three pillars: team, program, and portfolio.
            video:
              video_url: https://www.youtube.com/embed/PmFFlTH2DQk?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: copy-media
      data:
        block:
          - header: Features
            miscellaneous: |
              *   **Issues:** Start with an [issue](https://docs.gitlab.com/ee/user/project/issues/){data-ga-name="start with an issue" data-ga-location="body"} that captures a single feature that delivers business value for users.
              *   **Tasks:** Often, a user story is further separated into individual tasks. You can create a task [list](https://docs.gitlab.com/ee/user/markdown.html#task-lists){data-ga-name="task list" data-ga-location="body"} within an issue's description in GitLab, to further identify those individual tasks.
              *   **Issue boards:** Everything is in one place. [Track](https://docs.gitlab.com/ee/user/project/issues/#issues-per-project){data-ga-name="track" data-ga-location="body"} issues and communicate progress without switching between products. One interface to follow your issues from backlog to done.
              *   **Epics:** Manage your portfolio of projects more efficiently and with less effort by tracking groups of issues that share a theme, across projects and milestones with [epics](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="milestones with epics" data-ga-location="body"}.
              *   **Milestones:** Track issues and merge requests created to achieve a broader goal in a certain period of time with GitLab [milestones](https://docs.gitlab.com/ee/user/project/milestones/){data-ga-name="gitlab milestones" data-ga-location="body"}.
              *   **Roadmaps:** Start date and/or due date can be visualized in a form of a timeline. The epics [roadmap](https://docs.gitlab.com/ee/user/group/roadmap/){data-ga-name="roadmap" data-ga-location="body"} page shows such a visualization for all the epics which are under a group and/or its subgroups.
              *   **Labels:** Create and assigned to individual issues, which then allows you to filter the issue lists by a single label or multiple [labels](https://docs.gitlab.com/ee/user/project/labels.html#prioritize-labels){data-ga-name="multiple labels" data-ga-location="body"}.
              *   **Burndown Chart:** Track work in real time, and mitigate risks as they arise. [Burndown](https://docs.gitlab.com/ee/user/project/milestones/burndown_and_burnup_charts.html#burndown-charts){data-ga-name="burndown" data-ga-location="body"} charts allow teams to visualize the work scoped in a current sprint as they are being completed.
              *   **Points and Estimation:** Indicate the estimated effort with issues by assigning [weight](https://docs.gitlab.com/ee/user/project/issues/issue_weight.html){data-ga-name="weight" data-ga-location="body"} attributes and indicate estimated effort
              *   **Collaboration:** The ability to [contribute](https://docs.gitlab.com/ee/user/discussions/){data-ga-name="contribute" data-ga-location="body"} conversationally is offered throughout GitLab in issues, epics, merge requests, commits, and more!
              *   **Traceability:** Align your team's issues with subsequent [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/){data-ga-name="merge requests" data-ga-location="body"} that give you complete traceability from issue creation to end once the related pipeline passes.
              *   **Wikis:** A system for documentation called [Wiki](https://docs.gitlab.com/ee/user/project/wiki/){data-ga-name="wiki" data-ga-location="body"}, if you are wanting to keep your documentation in the same project where your code resides.
              *   **Enterprise Agile Frameworks:** Large enterprises have adopted Agile at enterprise scale using a variety of frameworks. GitLab can support [SAFe](https://www.scaledagileframework.com/), Spotify, [Disciplined Agile Delivery](https://www.disciplinedagiledelivery.com/) and more.
    - name: 'copy-media'
      data:
        block:
          - header: An Agile iteration with GitLab
            subtitle: User stories → GitLab issues
            text: |
              In Agile, you often start with a user story that captures a single feature that delivers business value for users. In GitLab, a single issue within a project serves this purpose.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue-list.png
              alt: ""
          - subtitle: Task → GitLab task lists
            inverted: true
            text: |
              Often, a user story is further separated into individual tasks. You can create a task list within an issue's description in GitLab, to further identify those individual tasks.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/Tasks.png
              alt: ""
          - subtitle: Epics → GitLab epics
            text: |
              In the other direction, some Agile practitioners specify an abstraction above user stories, often called an epic, that indicates a larger user flow consisting of multiple features. In GitLab, an epic also contains a title and description, much like an issue, but it allows you to attach multiple child issues to it to indicate that hierarchy.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue.png
              alt: ""
          - subtitle: Product backlog → GitLab issue lists and prioritized labels
            inverted: true
            text: |
              The product or business owners typically create these user stories to reflect the needs of the business and customers. They are prioritized in a product backlog to capture urgency and desired order of development. The product owner communicates with stakeholders to determine the priorities and constantly refines the backlog. In GitLab, there are dynamically generated issue lists which users can view to track their backlog. Labels can be created and assigned to individual issues, which then allows you to filter the issue lists by a single label or multiple labels. This allows for further flexibility. Priority labels can even be used to also order the issues in those lists.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue-board.png
              alt: ""
          - subtitle: Sprints → GitLab milestones
            text: |
              A sprint represents a finite time period in which the work is to be completed, which may be a week, a few weeks, or perhaps a month or more. The product owner and the development team meet to decide work that is in scope for the upcoming sprint. GitLab's milestones feature supports this: assign milestones a start date and a due date to capture the time period of the sprint. The team then puts issues into that sprint by assigning them to that particular milestone.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/Milestones.png
              alt: ""
          - subtitle: Points and estimation → GitLab issue weights
            inverted: true
            text: |
              Also in this meeting, user stories are communicated, and the level of technical effort is estimated for each in-scope user story. In GitLab, issues have a weight attribute, which you would use to indicate the estimated effort. In this meeting (or in subsequent ones), user stories are further broken down to technical deliverables, sometimes documenting technical plans and architecture. In GitLab, this information can be documented in the issue, or in the merge request description, as the merge request is often the place where technical collaboration happens. During the sprint (GitLab milestone), development team members pick up user stories to work on, one by one. In GitLab, issues have assignees. So you would assign yourself to an issue to reflect that you are now working on it. We'd recommend that you create an empty and linked-to-issue merge request right away to start the technical collaboration process, even before creating a single line of code.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/weight.png
              alt: ""
          - subtitle: Agile board → GitLab Issue Boards
            text: |
              Throughout the sprint, issues move through various stages, such as Ready for dev, In dev, In QA, In review, Done, depending on the workflow in your particular organization. Typically these are columns in an Agile board. In GitLab, issue boards allow you to define your stages and enable you to move issues through them. The team can configure the board with respect to the milestone and other relevant attributes. During daily standups, the team looks at the board together, to see the status of the sprint from a workflow perspective.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue_board.png
              alt: ""
          - subtitle: Burndown charts → GitLab Burndown Charts
            inverted: true
            text: |
              The development team wants to know if they are on track in real time, and mitigate risks as they arise. GitLab provides burndown charts, allowing the team to visualize the work scoped in the current sprint "burning down" as they are being completed. Toward the end of the sprint, the development team demos completed features to various stakeholders. With GitLab, this process is made simple using Review Apps so that even code not yet released to production, but in various testing, staging or UAT environments can be demoed. Review Apps and CI/CD features are integrated with the merge request itself. These same tools are useful for Developers and QA roles to maintain software quality, whether through automated testing with CI/CD, or manual testing in a Review App environment.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/burndown-chart.png
              alt: ""
    - name: copy-resources
      data:
        block:
          - subtitle: Are you ready for the next step?
            text: |
              Please see [get help for GitLab](/get-help/){data-ga-name="get help" data-ga-location="body"} if you have questions
            resources:
              video:
                header: Videos
                links:
                  - text: View a guide to setting up agile teams with GitLab
                    link: https://youtu.be/VR2r1TJCDew
                  - text: How to do SAFe (Scaled Agile Framework) with GitLab
                    link: https://youtu.be/PmFFlTH2DQk
                  - text: How Issue Boards work with GitLab
                    link: https://youtu.be/CiolDtBIOA0
                  - text: Configurable Issue Boards
                    link: https://youtu.be/m5UTNCSqaDk
              blog:
                header: Blogs
                links:
                  - text: How to use GitLab for Agile
                    link: /blog/2018/03/05/gitlab-for-agile-software-development/
                    data_ga_name: GitLab for Agile
                    data_ga_location: body
                  - text: 4 ways to use Boards
                    link: /blog/2018/08/02/4-ways-to-use-gitlab-issue-boards/
                    data_ga_name: 4 ways to use Boards
                    data_ga_location: body
